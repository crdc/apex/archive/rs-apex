extern crate zmq;

fn main() {
    let ctx = zmq::Context::new();

    let sock = ctx.socket(zmq::REQ).unwrap();
    sock.connect("tcp://127.0.0.1:7220").unwrap();
    sock.send(b"{ \"id\": \"test\" }", 0).unwrap();
    let msg = zmq::Message::new();
    let mut msg = msg.unwrap();
    sock.recv(&mut msg, 0).unwrap();
    println!("Received {}", msg.as_str().unwrap());
}
