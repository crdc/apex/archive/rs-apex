[![codecov](https://codecov.io/gl/apex/apex-core/branch/master/graph/badge.svg)](https://codecov.io/gl/apex/apex-core)

# Apex Core

Libraries and core services for Apex measurement, analysis, and control systems.

## Building

```sh
git clone --recurse-submodules https://gitlab.com/crdc/apex/apex-core
cd apex-core/
rustup default nightly
cargo build
CONF_DIR=./data/config ./target/debug/apex-acquire -vvv
```

## Testing

This should work...

```sh
pip install --user zmqc
```

But it didn't, so installing manually was necessary.

```sh
git clone https://github.com/zacharyvoase/zmqc
cd zmqc/
sudo python setup.py install
```
