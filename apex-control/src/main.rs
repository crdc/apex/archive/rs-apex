//! Apex acquisition service
//!
//! This application is used to ingest and record data from a variety of sources
//! This application is used to ingest data from a variety of sources, perform
//! control tasks, and send it back out over ZMQ sockets, most commonly REQ/REP
//! and PUB/SUB.

extern crate apex;

#[macro_use]
extern crate log;
extern crate env_logger;
extern crate serde_json;

use std::error::Error;
use std::process;

use apex::message::Message;

fn main() {
    env_logger::init();

    match run() {
        Ok(_) => {}
        Err(e) => {
            error!("error: {}", e);
            process::exit(1);
        }
    }
}

fn run() -> Result<(), Box<Error>> {
    inform("apex-control".to_string())?;

    Ok(())
}

fn inform(name: String) -> Result<(), Box<Error>> {
    // Broadcast notify message to inform network
    // TODO: get ID from config
    let message = Message { id: name, };
    info!("{}", serde_json::to_string_pretty(&message)?);

    Ok(())
}
