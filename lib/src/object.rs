use std::io::Cursor;

use prost::Message;

use apex::core::v1::Object;

impl Object {
    pub fn new() -> Object {
        Object::default()
    }

    pub fn serialize(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.reserve(self.encoded_len());
        self.encode(&mut buf).unwrap();
        buf
    }

    pub fn deserialize(&self, buf: &[u8]) -> Result<Object, prost::DecodeError> {
        Object::decode(&mut Cursor::new(buf))
    }
}

#[cfg(test)]
mod tests {
    //use super::*;

    #[test]
    fn can_serialize() {
        //let mut object = Object::new();
        //let bytes = vec![
        //];

        //object. = "".to_string();

        //let buf = object.serialize();

        //assert_eq!(buf, bytes);
    }

    #[test]
    fn can_deserialize() {
        //let mut object = Object::new();
        //let bytes = vec![
        //];

        //let msg = object.deserialize(&bytes)
            //.expect("Failed to deserialize object");

        //assert_eq!(msg., "");
    }
}
