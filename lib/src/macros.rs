#![macro_use]

#[macro_export]
macro_rules! impl_msg {
    ($name:ident { $($field:ident),* }) => {
        impl $name {
            fn new() -> $name {
                $name::default()
            }

            fn serialize(&self) -> Vec<u8> {
                let mut buf = Vec::new();
                buf.reserve(self.encoded_len());
                self.encode(&mut buf).unwrap();
                buf
            }

            fn deserialize(&self, buf: &[u8]) -> Result<$name, prost::DecodeError> {
                $name::decode(&mut Cursor::new(buf))
            }
        }
    };
}
