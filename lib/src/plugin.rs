use std::io::Cursor;

use prost::Message;

use apex::core::v1::{Plugin, PluginRequest, PluginListReply};

//impl_msg!(Plugin);
#[allow(dead_code)]
impl Plugin {
    fn new() -> Plugin {
        Plugin::default()
    }

    fn serialize(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.reserve(self.encoded_len());
        self.encode(&mut buf).unwrap();
        buf
    }

    fn deserialize(&self, buf: &[u8]) -> Result<Plugin, prost::DecodeError> {
        Plugin::decode(&mut Cursor::new(buf))
    }
}

#[allow(dead_code)]
impl PluginRequest {
    fn new() -> PluginRequest {
        PluginRequest::default()
    }

    fn serialize(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.reserve(self.encoded_len());
        self.encode(&mut buf).unwrap();
        buf
    }

    fn deserialize(&self, buf: &[u8]) -> Result<PluginRequest, prost::DecodeError> {
        PluginRequest::decode(&mut Cursor::new(buf))
    }
}

#[allow(dead_code)]
impl PluginListReply {
    fn new() -> PluginListReply {
        PluginListReply::default()
    }

    fn serialize(&self) -> Vec<u8> {
        let mut buf = Vec::new();
        buf.reserve(self.encoded_len());
        self.encode(&mut buf).unwrap();
        buf
    }

    fn deserialize(&self, buf: &[u8]) -> Result<PluginListReply, prost::DecodeError> {
        PluginListReply::decode(&mut Cursor::new(buf))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_serialize() {
        let mut plugin = Plugin::new();
        let bytes = vec![
            10, 16, 97, 112, 101, 120, 45, 112, 108, 117, 103,
            105, 110, 45, 116, 101, 115, 116, 18, 12, 97, 112,
            101, 120, 45, 97, 99, 113, 117, 105, 114, 101, 24,
            1, 32, 1, 40, 1
        ];

        plugin.name = "apex-plugin-test".to_string();
        plugin.service_name = "apex-acquire".to_string();
        plugin.loaded = true;
        plugin.registered = true;
        plugin.enabled = true;

        let buf = plugin.serialize();

        assert_eq!(buf, bytes);
    }

    #[test]
    fn can_deserialize() {
        let plugin = Plugin::new();
        let bytes = vec![
            10, 16, 97, 112, 101, 120, 45, 112, 108, 117, 103,
            105, 110, 45, 116, 101, 115, 116, 18, 12, 97, 112,
            101, 120, 45, 97, 99, 113, 117, 105, 114, 101, 24,
            1, 32, 1, 40, 1
        ];

        let msg = plugin.deserialize(&bytes)
            .expect("Failed to deserialize plugin");

        assert_eq!(msg.name, "apex-plugin-test");
        assert_eq!(msg.service_name, "apex-acquire");
        assert_eq!(msg.loaded, true);
        assert_eq!(msg.registered, true);
        assert_eq!(msg.enabled, true);
    }
}
